import { Observables3Page } from './app.po';

describe('observables3 App', () => {
  let page: Observables3Page;

  beforeEach(() => {
    page = new Observables3Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
