import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/multicast';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app works Arnau!';
  private data: any;

  constructor() {
    // this.data = new Observable(observer => {
    //   setTimeout(() => {
    //     observer.next(21);
    //   }, 1000);
    //   setTimeout(() => {
    //     observer.next(22);
    //   }, 1000);
    //   setTimeout(() => {
    //     observer.complete();
    //   }, 1000);
    // });

    // let newobs = Observable.interval(3000).take(5).map(x => x * 5);

    // let newsub = newobs.subscribe(
    //   x => console.log(x),
    //   () => console.log('done')
    // );

    // let subscriber = this.data.subscribe(
    //   value => console.log(value),
    //   err => console.log(err),
    //   () => console.log('done')
    // );
    let source = Observable.interval(500);
    let subject = new Subject();
    let refCounted = source.multicast(subject).refCount();
    let subscription1, subscription2, subscriptionConnect;

    // This calls `connect()`, because
    // it is the first subscriber to `refCounted`
    console.log('observerA subscribed');
    subscription1 = refCounted.subscribe({
      next: (v) => console.log('observerA: ' + v)
    });

    setTimeout(() => {
      console.log('observerB subscribed');
      subscription2 = refCounted.subscribe({
        next: (v) => console.log('observerB: ' + v)
      });
    }, 600);

    setTimeout(() => {
      console.log('observerA unsubscribed');
      subscription1.unsubscribe();
    }, 1200);

    // This is when the shared Observable execution will stop, because
    // `refCounted` would have no more subscribers after this
    setTimeout(() => {
      console.log('observerB unsubscribed');
      subscription2.unsubscribe();
    }, 2000);

    let input = Observable.from([1, 2, 3, 4]);
    let output = this.multiplyByTen(input).map(x => x);
    output.subscribe(x => console.log(x));

  }

  multiplyByTen(input) {
    let output = Observable.create(function subscribe(observer) {
      input.subscribe({
        next: (v) => observer.next(10 * v),
        error: (err) => observer.error(err),
        complete: () => observer.complete()
      });
    });
    return output;
  }



}
